package com.training.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.training.model.Adresse;
import com.training.model.Employee;
import com.training.repo.AdresseRepository;
import com.training.repo.EmployeeRepository;

public class AdresseService {
	
	@Autowired
	private AdresseRepository adresseRepository;

	public Page<Adresse> findAll(Pageable pageable){
		return this.adresseRepository.findAll(pageable);
	};
	
}
